﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using TMPro;
public class GeneticAlgorithm : MonoBehaviour
{

    public static GeneticAlgorithm instance;

    public int populationSize = 50;
    public float mutationRate = 0.02f;

    public GameObject CarPrefab;
    public TextMeshProUGUI genText;

    private Car[] cars;

    private int deadPlayers = 0;

    private float fitnessSum = 0;
    private int generation = 0;
    private int greatestIndex = -1;

    private int trashCutOff;

    private GameObject greatestPlayer;
    // Start is called before the first frame update
    void Awake()
    {
        if (instance)
        {
            Destroy(gameObject);
        }
        else
        {
            genText.text = "Generation: " + 0;
            instance = this;          
        }
        
    }

    public void StartDriving()
    {
        cars = new Car[populationSize];
        trashCutOff = (int)(populationSize * 0.9f);

        for (int i = 0; i < populationSize; i++)
        {
            GameObject obj = Instantiate(CarPrefab);
            obj.transform.position = Vector3.zero;
            obj.name = i.ToString();
            cars[i] = obj.GetComponent<Car>();
        }
    }

    public void Restart()
    {
        deadPlayers = 0;
        fitnessSum = 0;
        greatestIndex = -1;

        generation++;
        genText.text = "Generation: " + generation;

        for (int i = 0; i < populationSize; i++)
        {
            cars[i].Reset();
        }
    }

    public void NewGeneration()
    {
        for (int i = 0; i < populationSize; i++) { 

            if (!cars[i].isDead)
            {
                cars[i].overallFitness = 0;
                cars[i].isDead = true;
                if(deadPlayers + 1 == populationSize)
                {
                    KillCar();
                    return;
                }
                KillCar();
            }
          
        }
    }

    public void KillCar()
    {
        deadPlayers++;
        if(deadPlayers == populationSize)
        {           
            ArrangeArray();
            SelectNextGen();            
            Mutate();            
            Restart();           
        }
    }

    void CalculateFitnessForAllPlayers()
    {
        fitnessSum = 0;
        for (int i = 0; i < populationSize; i++)
        {
            fitnessSum += cars[i].overallFitness;
        }
    }

    int GetBestInGeneration()
    {
        float maxFit = 0;
        int maxFitIndex = 0;
        for (int i = 0; i < populationSize; i++)
        {
            if (cars[i].overallFitness > maxFit)
            {
                maxFit = cars[i].overallFitness;
                maxFitIndex = i;
            }
        }
        return maxFitIndex;
    }

    void ArrangeArray()
    {
        cars = cars.OrderBy(x => x.overallFitness).ToArray();
    }

    void SelectNextGen()
    {     
        for (int i = 0; i < trashCutOff; i++)
        {
            int a = Random.Range(trashCutOff, cars.Length);
            int b = Random.Range(trashCutOff, cars.Length);
            cars[i].SetNetwork(cars[a].MakeABaby(cars[b]));
        }
    }

    void Mutate()
    {
        //Mutate all but the best car
        for(int i = 3; i < cars.Length - 1; ++i)
        {
            cars[i].Mutate(mutationRate);
        }

        //Mutate all but the best car
        for (int i = 0; i < 3; ++i)
        {
            cars[i].network.weights[0].Randomize();
            cars[i].network.weights[1].Randomize();
            cars[i].network.weights[2].Randomize();
        }

    }

}
