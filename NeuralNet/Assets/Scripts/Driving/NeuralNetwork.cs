﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class NeuralNetwork : MonoBehaviour
{
    public Matrix input = new Matrix(1, 5);
    public List<Matrix> hiddenLayers = new List<Matrix>();
    public Matrix outputLayer = new Matrix(1, 2);

    public List<Matrix> weights = new List<Matrix>();
    public List<float> biases = new List<float>();

    public float fitness;

    private int hiddenLayersCount, hiddenNeuronCount;

    public void Init(int hiddenLayersCount, int hiddenNeuronCount)
    {
        input.Clear();
        hiddenLayers.Clear();
        outputLayer.Clear();
        weights.Clear();
        biases.Clear();

        this.hiddenLayersCount = hiddenLayersCount;
        this.hiddenNeuronCount = hiddenNeuronCount;


        for(int i = 0; i < hiddenLayersCount; i++)
        {
            Matrix f = new Matrix(1, hiddenNeuronCount);
            hiddenLayers.Add(f);

            biases.Add(Random.Range(-1f, 1f));

            //Weights
            if(i == 0)
            {
                Matrix inputToHidden = new Matrix(5, hiddenNeuronCount);
                weights.Add(inputToHidden);
            }

            Matrix HiddenToHidden = new Matrix(hiddenNeuronCount, hiddenNeuronCount);
            weights.Add(HiddenToHidden);

        }

        Matrix OutputWeight = new Matrix(hiddenNeuronCount, 2);
        weights.Add(OutputWeight);
        biases.Add(Random.Range(-1f, 1f));

        RandomWeightInit();
    }

    void RandomWeightInit()
    {
        for (int i = 0; i < weights.Count; ++i)
        {
            weights[i].Randomize();
        }
    }

    public (float, float) RunNetwork(float a, float b, float c, float d, float e)
    {
        input.SetRow(0, a, b, c,d,e);
        input.PointwiseTanh();

        //Debug.Log(input.ToString());

        hiddenLayers[0] = ((input * weights[0]) + biases[0]);
        hiddenLayers[0].PointwiseTanh();

        for(int i = 1; i < hiddenLayers.Count; i++)
        {
            hiddenLayers[i] = ((hiddenLayers[i - 1] * weights[i]) + biases[i]);
            hiddenLayers[i].PointwiseTanh();
        }

        outputLayer = (hiddenLayers[hiddenLayers.Count - 1] * weights[weights.Count - 1]) + biases[biases.Count - 1];
        outputLayer.PointwiseTanh();

        //First is acceleration and second output is steering
        return (MathFunctions.Sigmoid(outputLayer.matrix[0, 0]), MathFunctions.TanH(outputLayer.matrix[0, 1]));
    }

    public NeuralNetwork MakeABaby(NeuralNetwork b)
    {
        GameObject go = new GameObject();        
        NeuralNetwork baby = go.AddComponent<NeuralNetwork>();
        baby.Init(hiddenLayersCount, hiddenNeuronCount);

        for (int i = 0; i < weights.Count; ++i)
        { 
            for (int j = 0; j < weights[i].rows; ++j)
            {
                for (int k = 0; k < weights[i].cols; ++k)
                {
                    float num = Random.Range(0F, 1F);
                    if(num < 0.5f)
                    {
                        baby.weights[i].matrix[j,k] = weights[i].matrix[j, k];
                    }
                    else
                    {
                        baby.weights[i].matrix[j, k] = b.weights[i].matrix[j, k];
                    }
                   
                }
            }
        }

        for(int i = 0; i < biases.Count; i++)
        {
            float num = Random.Range(0F, 1F);
            if (num < 0.5f)
            {
                baby.biases[i] = biases[i];
            }
            else
            {
                baby.biases[i] = b.biases[i];
            }
        }


        return baby;
    }
}
