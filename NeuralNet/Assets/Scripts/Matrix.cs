﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[System.Serializable]
public class Matrix
{
    [SerializeField]
    public float[,] matrix { get; private set; }
    public int rows { get; private set; }
    public int cols { get; private set; }

    public Matrix(int rows, int cols)
    {
        matrix = new float[rows, cols];
        this.rows = rows;
        this.cols = cols;
    }

    public void Clear()
    {
        for (uint i = 0; i < rows; ++i)
        {
            for (uint j = 0; j < cols; ++j)
            {
                matrix[i, j] = 0;
            }
        }
    }

    public void SetRow(int rowNum, params float[] values)
    {
        if (values.Length - 1 > cols || rowNum > rows) { Debug.LogError("Invalid input"); return; }
       
        for(uint i  = 0; i < values.Length; ++i)
        {
            matrix[rowNum, i] = values[i];
        }
    }

    public void SetCol(int colNum, params float[] values)
    {
        if (values.Length - 1 > rows || colNum > cols) { Debug.LogError("Invalid input"); return;}

        for (uint i = 0; i < values.Length; ++i)
        {
            matrix[i, colNum] = values[i];
        }
    }


    public override string ToString()
    {
        string s = "";
        for(uint i = 0; i < rows; ++i)
        {
            for(uint j = 0; j < cols; ++j)
            {
                s += matrix[i, j] + " ";
            }
            s += "\n";
        }

        return s;
    }

    public void PointwiseTanh()
    {
        for (uint i = 0; i < rows; ++i)
        {
            for (uint j = 0; j < cols; ++j)
            {
                matrix[i, j] = MathFunctions.TanH(matrix[i, j]);
            }
        }
    }

    public void Randomize()
    {
        for (uint i = 0; i < rows; ++i)
        {
            for (uint j = 0; j < cols; ++j)
            {
                matrix[i, j] = Random.Range(-1f,1f);
            }
        }
    }

    public static Matrix operator *(Matrix left, Matrix right)
    {
        if (left.cols != right.rows) { Debug.LogError("Invalid MAtrix Sizes"); return null; }

        Matrix matrix = new Matrix(left.rows, right.cols);

        for (uint i = 0; i < left.rows; ++i)
        {
            for (uint j = 0; j < right.cols; ++j)
            {
                for (uint k = 0; k < left.cols; ++k)
                {
                    matrix.matrix[i,j] += left.matrix[i,k] * right.matrix[k,j];
                }
            }
        }

        return matrix;
    }

    public static Matrix operator +(Matrix a, float right)
    {
        Matrix val = a;
        for (uint i = 0; i < val.rows; ++i)
        {
            for (uint j = 0; j < val.cols; ++j)
            {
                val.matrix[i, j] += right;
            }
        }
        return val;
    }


}
