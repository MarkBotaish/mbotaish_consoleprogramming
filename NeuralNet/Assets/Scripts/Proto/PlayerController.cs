﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    private Rigidbody rb;
    private PlayerMovement pm;

    private bool isDead = false;
    private bool hasReachedGoal = false;

    public bool IsDead() { return isDead; }
    public bool HasReachedGoal() { return hasReachedGoal; }

    public void Init(PlayerMovement pm)
    {
        this.pm = pm;
        rb = GetComponent<Rigidbody>();
    }

    public void Restart()
    {
        isDead = false;
        hasReachedGoal = false;

    }


    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Wall" && !isDead)
        {
            isDead = true;
            rb.velocity = Vector3.zero;
            pm.AddToDeathCount();
        }

        if (collision.gameObject.tag == "Goal" && !isDead)
        {
            hasReachedGoal = true;
            isDead = true;
            rb.velocity = Vector3.zero;
            pm.AddToDeathCount();
        }
    }
}
