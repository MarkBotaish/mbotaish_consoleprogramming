﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Brain
{
    public List<Vector3> inputs;
    int size = 0;

    public List<Vector3> GetInput() { return inputs; }

    public void Init(int size)
    {
        this.size = size;  
        inputs = new List<Vector3>(new Vector3[size]);
        RandomInput();
    }

    public Vector3 GetNextMove(int step)
    {
        if(step < inputs.Count)
        {           
            return inputs[step];
        }
        return inputs[size - 1];
    }

    private void RandomInput()
    {
        Vector3 temp = Vector3.zero;
      
        for(int i = 0; i < inputs.Count; i++)
        {
            if(i%2 == 0)
            {
                temp.x = Random.Range(-2, 3);
                inputs[i] = temp;
            }
            else
            {
                inputs[i] = Vector3.zero;
            }
           
        }
    } 

   public void mutate()
    {
        Vector3 temp = Vector3.zero;
        float mutationRate = 0.1f;
        for (int i = 0; i < inputs.Count; i++)
        {
            float random = Random.Range(0.0f, i);
            if(random < mutationRate)
            {
                temp.x = Random.Range(-2, 3);
                inputs[i] = temp;
            }           
        }
    }


}
