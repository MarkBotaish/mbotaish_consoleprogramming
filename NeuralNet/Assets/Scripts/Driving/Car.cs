﻿using UnityEngine;

public class Car : MonoBehaviour
{
    private Vector3 startPosition;
    private Vector3 startRotation;

    [HideInInspector] public NeuralNetwork network;

    [Range(-1f, 1f)]
    [HideInInspector]  public float acceleration, turning;

    private float timeSinceStart = 0f;

    [Header("Fitness")]
    [HideInInspector] public float overallFitness;
    private float distanceMultipler = 1.4f;
    private float avgSpeedMultilier = 0.2f;
    private float sensorMultiplier = 0.1f;

    private Vector3 lastPosition;
    private float totalDistanceTravelled;
    private float avgSpeed;

    private float aSensor, bSensor, cSensor, dSensor, eSensor;

    private Vector3 inp;

    [Header("Network Options")]
    [SerializeField] private int LAYERS = 1;
    [SerializeField] private int NEURONS = 10;

    private LayerMask layerMask;
    public bool isDead = false;

    private void Awake()
    {
        startPosition = transform.position;
        startRotation = transform.eulerAngles;

        network = GetComponent<NeuralNetwork>();
        network.Init(LAYERS, NEURONS);
        layerMask = LayerMask.GetMask("Player");
    }

    public void SetNetwork(NeuralNetwork network)
    {
        this.network = network;
    }

    public void Reset()
    {
        timeSinceStart = 0F;
        totalDistanceTravelled = 0;
        avgSpeed = 0;
        lastPosition = startPosition;
        overallFitness = 0;
        transform.position = startPosition;
        transform.eulerAngles = startRotation;
        //network.Init(LAYER, NEURONS);
        isDead = false;
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (!isDead)
        {
            isDead = true;
            GeneticAlgorithm.instance.KillCar();
        }
    }

    private void FixedUpdate()
    {
        if (isDead) { return; }

        InputSensor();
        lastPosition = transform.position;

        (acceleration, turning) = network.RunNetwork(aSensor, bSensor, cSensor, dSensor, eSensor);

        MoveCar(acceleration, turning);

        timeSinceStart += Time.deltaTime;
        CalculateFitness();

        acceleration = 0;
        turning = 0;
    }

    private void CalculateFitness()
    {
        totalDistanceTravelled += Vector3.Distance(transform.position, lastPosition);
        avgSpeed = totalDistanceTravelled / timeSinceStart;

        overallFitness = (totalDistanceTravelled * distanceMultipler) + (avgSpeed * avgSpeedMultilier) + (((aSensor + bSensor + cSensor + dSensor + eSensor) / 5.0f) * sensorMultiplier);
    }

    private void InputSensor()
    {
        Vector3 a = transform.forward + transform.right;
        Vector3 b = transform.forward;
        Vector3 c = transform.forward - transform.right;
        Vector3 d = transform.right;
        Vector3 e = - transform.right;

        Ray r = new Ray(transform.position, a);
        RaycastHit hit;

        if (Physics.Raycast(r, out hit, Mathf.Infinity, ~layerMask))
        {
            aSensor =  hit.distance / 20.0f; //Might change to sigmod function
            Debug.DrawRay(r.origin, hit.point - r.origin, Color.red);
        }

        r.direction = b;

        if (Physics.Raycast(r, out hit, Mathf.Infinity, ~layerMask))
        {
            bSensor = hit.distance / 20.0f; //Might change to sigmod function
            Debug.DrawRay(r.origin, hit.point - r.origin, Color.blue);
        }

        r.direction = c;

        if (Physics.Raycast(r, out hit, Mathf.Infinity, ~layerMask))
        {
            cSensor = hit.distance / 20.0f; //Might change to sigmod function
            Debug.DrawRay(r.origin, hit.point - r.origin, Color.red);
        }

        r.direction = d;

        if (Physics.Raycast(r, out hit, Mathf.Infinity, ~layerMask))
        {
            dSensor = hit.distance / 20.0f; //Might change to sigmod function
            Debug.DrawRay(r.origin, hit.point - r.origin, Color.red);
        }

        r.direction = e;

        if (Physics.Raycast(r, out hit, Mathf.Infinity, ~layerMask))
        {
            eSensor = hit.distance / 20.0f; //Might change to sigmod function
            Debug.DrawRay(r.origin, hit.point - r.origin, Color.red);
        }

    }

    //rotation and acceleration
    public void MoveCar(float v, float h)
    {
        inp = Vector3.Lerp(Vector3.zero, new Vector3(0, 0, v * 11.4f), 0.02f);
        inp = transform.TransformDirection(inp);
        transform.position += inp;

        transform.eulerAngles += new Vector3(0, h * 90 * 0.02f, 0);
    }

    public NeuralNetwork MakeABaby(Car a)
    {
        return network.MakeABaby(a.network);
    }

    public void Mutate(float rate)
    {

        for (int i = 0; i < network.weights.Count; i++)
        {
            if (Random.Range(0.0f, 1.0f) < rate)
            {
                int points = network.weights[i].rows * network.weights[i].cols / 2;
                print("MUTATE " + points);
                for (int j = 0; j < points; j++)
                {
                    int RandomCol = Random.Range(0, network.weights[i].cols);
                    int RandomRow = Random.Range(0, network.weights[i].rows);

                    network.weights[i].matrix[RandomRow, RandomCol] = Mathf.Clamp(network.weights[i].matrix[RandomRow, RandomCol] + Random.Range(-1f, 1f), -1f, 1f);
                }
            }
        }
    }
}