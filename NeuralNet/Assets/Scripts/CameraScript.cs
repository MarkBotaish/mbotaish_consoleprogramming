﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraScript : MonoBehaviour
{

    private float speed = 0.1f;
    private Camera cam;

    public float dragSpeed = 2;
    private Vector3 dragOrigin;

    private void Start()
    {
        cam = GetComponent<Camera>();
    }

    private void FixedUpdate()
    {
        if (Input.GetKey(KeyCode.W))
        {
            transform.position += Vector3.forward * speed;
        }

        if (Input.GetKey(KeyCode.S))
        {
            transform.position -= Vector3.forward * speed;
        }

        if (Input.GetKey(KeyCode.A))
        {
            transform.position -= Vector3.right * speed;
        }

        if (Input.GetKey(KeyCode.D))
        {
            transform.position += Vector3.right * speed;
        }

        float wheel = Input.GetAxis("Mouse ScrollWheel");
        if (wheel > 0)
        {
            cam.orthographicSize += speed * 2;
        }

        if(wheel < 0)
        {
            cam.orthographicSize += speed * 2;
        }

    }


}
