﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class MathFunctions
{
    public static float TanH(float x)
    {
        return (Mathf.Exp(2 * x) - 1) / (Mathf.Exp(2 * x) + 1);
    }

    public static float Sigmoid(float x)
    {
        return (1 / (1 + Mathf.Exp(-x)));
    }
}
