﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GenerateMap : MonoBehaviour
{

    public GameObject Prefab;
    public Sprite map;

    [ContextMenu("GENERATE")]
    public void GenerateMapFromSprite()
    {
        Texture2D texture = map.texture;

        for(int i = 0; i < texture.width; i++)
        {
            for(int j = 0; j < texture.height; j++)
            {
                if(texture.GetPixel(i, j) == Color.black)
                {
                    GameObject obj = Instantiate(Prefab, new Vector3(i, 0, j), Quaternion.identity);
                    obj.transform.parent = transform;
                }
            }
        }

    }
}
