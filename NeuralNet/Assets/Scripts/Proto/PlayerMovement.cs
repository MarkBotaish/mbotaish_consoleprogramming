﻿    using UnityEngine;

[System.Serializable]
public class PlayerMovement
{

    private Rigidbody rb;
    private Vector3 gravity;
    public Brain brain;
    private Vector3 goal;

    private float fitness = 0;
    private int step = 0;
    private int counterStep = 0;
    private Population controller;
    private GameObject obj;
    private PlayerController pc;

    public GameObject GetObject() { return obj; }

    public void InitOnce(Vector3 goal, Population controller)
    {
        brain = new Brain();
        brain.Init(400);
        gravity = -Vector3.forward * 5;

        this.goal = goal;
        this.controller = controller;
    }

    // Start is called before the first frame update
    public void init(GameObject obj)
    {
        //Vector3
        this.obj = obj;
        rb = obj.GetComponent<Rigidbody>();
        pc = obj.GetComponent<PlayerController>();
      
        rb.velocity = gravity;
        pc.Init(this);            
    }

    public void Restart()
    {
        pc.Restart();
    }

    public void AddToDeathCount()
    {
        controller.AddToDeathCount();
    }

    // Update is called once per frame
    public void UpdateInputs()
    {
        if (!pc.IsDead() && !pc.HasReachedGoal())
        {
            Vector3 nextStep = brain.GetNextMove(step);
            if (nextStep == Vector3.zero) { counterStep++; }
            rb.velocity += brain.GetNextMove(step);
            step++;
        }       
    }

    public float CalcualteFitness()
    {
        if (pc.HasReachedGoal())
        {
            float diffInStep = step - counterStep;
            if (diffInStep == 0) { diffInStep = 0.0001f; }
            fitness = (1.0F/16.0F) + 10000.0F / ((float)(diffInStep * diffInStep));
        }
        else
        {
            float distToZ = (obj.transform.position.z - goal.z);
            fitness = 1 / (distToZ * distToZ);          
        }
        
        return fitness;
    }

    public PlayerMovement GetBaby()
    {
        PlayerMovement player = new PlayerMovement();
        player.InitOnce(goal, controller);
        player.brain.inputs = new System.Collections.Generic.List<Vector3>(brain.inputs);
        return player;
    }

   

    public void Mutate()
    {
        brain.mutate();
    }
}
