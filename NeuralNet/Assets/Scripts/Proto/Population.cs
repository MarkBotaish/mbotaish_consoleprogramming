﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Population : MonoBehaviour
{
    public Text generationText;
    public Transform start;
    public Transform goal;
    public GameObject playerPrefab;
    public int populationSize = 20;


    private int playersDead = 0;
    private PlayerMovement[] players;
    private PlayerMovement greatestPlayer;

    private float fitnessSum = 0;
    private int generation = 0;
    private int greatestIndex = -1;
    // Start is called before the first frame update
    void Start()
    {
        players = new PlayerMovement[populationSize];
        for (int i = 0; i < populationSize; i++)
        {
            GameObject obj = Instantiate(playerPrefab);
            obj.name += " " + i;              
            obj.transform.position = start.transform.position;
            obj.transform.SetParent(transform);
            players[i] = new PlayerMovement();
            players[i].InitOnce(goal.position, this);
            players[i].init(obj);              
        
        }
       // Debug.Break();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        for (int i = 0; i < populationSize; i++)
        {
            players[i].UpdateInputs();
        }

        if(playersDead == populationSize)
        {
            SelectNextGen();
        }
    }

   private void RestartPopulation()
    {
        for (int i = 0; i < populationSize; i++)
        {
            players[i].GetObject().transform.position = new Vector3(start.transform.position.x, players[i].GetObject().transform.position.y, start.transform.position.z);
            players[i].Restart();
        }
        playersDead = 0;
    }

    void calculateFitnessForAllPlayers()
    {
        fitnessSum = 0;
        for (int i = 0; i < populationSize; i++)
        {
            fitnessSum += players[i].CalcualteFitness();
        }
    }

    public void AddToDeathCount()
    {
        playersDead++;
    }

    void SelectNextGen()
    {
        if (greatestIndex >= 0)
        {
            greatestPlayer.GetObject().GetComponent<MeshRenderer>().material.color = Color.black;
            greatestPlayer.GetObject().transform.position -= new Vector3(0, 0.1f, 0);
        }

        PlayerMovement[] newPlayers = new PlayerMovement[populationSize];
        calculateFitnessForAllPlayers();
        greatestIndex = GeteBestInGeneration();
        greatestPlayer = players[greatestIndex];

        newPlayers[greatestIndex] = players[greatestIndex].GetBaby();
        newPlayers[greatestIndex].init(players[greatestIndex].GetObject());
        newPlayers[greatestIndex].GetObject().GetComponent<MeshRenderer>().material.color = Color.blue;
        newPlayers[greatestIndex].GetObject().transform.position += new Vector3(0, 0.1f, 0);
        newPlayers[greatestIndex].GetObject().transform.SetAsFirstSibling();

        for (int i = 0; i < newPlayers.Length; i++)
        {
            if(i != greatestIndex)
            {
                PlayerMovement parent = GetParent();
                newPlayers[i] = parent.GetBaby();          
              
                newPlayers[i].init(players[i].GetObject());
                newPlayers[i].Mutate();
            }              
        }
        
        players = newPlayers;
        RestartPopulation();
        generation++;
        generationText.text = "Generation = " + generation;
    }

    PlayerMovement GetParent()
    {
        float randomFit = Random.Range(0, fitnessSum);
        float currentSum = 0;

        for (int i = 0; i < populationSize; i++)
        {
            currentSum += players[i].CalcualteFitness();
            if(currentSum > randomFit)
            {
                return players[i];
            }
        }

        return null;
    }

    int GeteBestInGeneration()
    {
        float maxFit = 0;
        int maxFitIndex = 0;
        for (int i = 0; i < populationSize; i++)
        {
           if(players[i].CalcualteFitness() > maxFit)
           {
               maxFit = players[i].CalcualteFitness();
               maxFitIndex = i; 
           }
        }
        return maxFitIndex;
    }
}
