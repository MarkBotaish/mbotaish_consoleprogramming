﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowMap : MonoBehaviour
{

    public GameObject MapMaker;

    private CreateMapManager mapCreator;
    // Start is called before the first frame update
    void Start()
    {
        mapCreator = MapMaker.GetComponent<CreateMapManager>();
    }

    // Update is called once per frame
    void Update()
    {
        gameObject.transform.position = Camera.main.WorldToScreenPoint(MapMaker.transform.position);
        Vector3 turnTo = mapCreator.GetCurrentRotation().eulerAngles;
        transform.eulerAngles = new Vector3(0, 0, -turnTo.y);
    }
}
