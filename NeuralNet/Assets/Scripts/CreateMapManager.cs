﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CreateMapManager : MonoBehaviour
{
    //turn left, forward, turn right
    public GameObject[] walls = new GameObject[3];

    private Vector3 currentPosition;
    private Quaternion rotation;
    void Start()
    {
        currentPosition = transform.position;
        rotation = transform.rotation;
    }

   public void AddToMap(int flag)
    {
        Create(walls[flag], flag - 1);
    }

    private void Create(GameObject prefab, float direction)
    {
        Instantiate(prefab, currentPosition, rotation);
        rotation *= Quaternion.Euler(0, 90 * direction, 0);
        currentPosition += rotation * Vector3.forward * 10;
        gameObject.transform.position = currentPosition;
        gameObject.transform.rotation = rotation;
    }

    public Quaternion GetCurrentRotation() { return rotation; }
}
